<?php
#parse("PHP File Header.php")

#if (${NAMESPACE})

namespace ${NAMESPACE};

#end

/** 
 * class ${NAME}
#if (${CONSTANT_1})
 * @method static ${NAME} ${CONSTANT_1}() Get new instance of ${CONSTANT_1} flag.
 * @method bool is${CONSTANT_1}() Value flag exactly equals ${CONSTANT_1} flag.
 * @method bool has${CONSTANT_1}() Value has ${CONSTANT_1} flag bit set.
 *
#set( $CONSTANT_1_VAL = "0b00000001" )
#set( $ALL="0b00000001" )
#end
#if (${CONSTANT_2})
 * @method static ${NAME} ${CONSTANT_2}() Get new instance of ${CONSTANT_2} flag.
 * @method bool is${CONSTANT_2}() Value flag exactly equals ${CONSTANT_2} flag.
 * @method bool has${CONSTANT_2}() Value has ${CONSTANT_2} flag bit set.
 *
#set( $CONSTANT_2_VAL = "0b00000010" )
#set( $ALL="0b00000011" )
#end
#if (${CONSTANT_3})
 * @method static ${NAME} ${CONSTANT_3}() Get new instance of ${CONSTANT_3} flag.
 * @method bool is${CONSTANT_3}() Value flag exactly equals ${CONSTANT_3} flag.
 * @method bool has${CONSTANT_3}() Value has ${CONSTANT_3} flag bit set.
 *
#set( $CONSTANT_3_VAL = "0b00000100" )
#set( $ALL="0b00000111" )
#end
#if (${CONSTANT_4})
 * @method static ${NAME} ${CONSTANT_4}() Get new instance of ${CONSTANT_4} flag.
 * @method bool is${CONSTANT_4}() Value flag exactly equals ${CONSTANT_4} flag.
 * @method bool has${CONSTANT_4}() Value has ${CONSTANT_4} flag bit set.
 *
#set( $CONSTANT_4_VAL = "0b00001000" )
#set( $ALL="0b00001111" )
#end
#if (${CONSTANT_5})
 * @method static ${NAME} ${CONSTANT_5}() Get new instance of ${CONSTANT_5} flag.
 * @method bool is${CONSTANT_5}() Value flag exactly equals ${CONSTANT_5} flag.
 * @method bool has${CONSTANT_5}() Value has ${CONSTANT_5} flag bit set.
 *
#set( $CONSTANT_5_VAL = "0b00010000" )
#set( $ALL="0b00011111" )
#end
#if (${CONSTANT_6})
 * @method static ${NAME} ${CONSTANT_6}() Get new instance of ${CONSTANT_6} flag.
 * @method bool is${CONSTANT_6}() Value flag exactly equals ${CONSTANT_6} flag.
 * @method bool has${CONSTANT_6}() Value has ${CONSTANT_6} flag bit set.
 *
#set( $CONSTANT_6_VAL = "0b00100000" )
#set( $ALL="0b00111111" )
#end
#if (${CONSTANT_7})
 * @method static ${NAME} ${CONSTANT_7}() Get new instance of ${CONSTANT_7} flag.
 * @method bool is${CONSTANT_7}() Value flag exactly equals ${CONSTANT_7} flag.
 * @method bool has${CONSTANT_7}() Value has ${CONSTANT_7} flag bit set.
 *
#set( $CONSTANT_7_VAL = "0b01000000" )
#set( $ALL="0b01111111" )
#end
#if (${CONSTANT_8})
 *
 * @method static ${NAME} ${CONSTANT_8}() Get new instance of ${CONSTANT_8} flag.
 * @method bool is${CONSTANT_8}() Value flag exactly equals ${CONSTANT_8} flag.
 * @method bool has${CONSTANT_8}() Value has ${CONSTANT_8} flag bit set.
 *
#set( $CONSTANT_8_VAL = "0b10000000" )
#set( $ALL="0b11111111" )
#end
 */
class ${NAME} extends \PhpEnumFlag {
#if (${ALL})
  /** @var int ALL ALL bitmask. */
  const ALL = ${ALL};
#end
#if (${CONSTANT_1})
  /** @var int ${CONSTANT_1} ${CONSTANT_1} flag. */
  const ${CONSTANT_1} = ${CONSTANT_1_VAL};
#end
#if (${CONSTANT_2})
  /** @var int ${CONSTANT_2} ${CONSTANT_2} flag. */
  const ${CONSTANT_2} = ${CONSTANT_2_VAL};
#end
#if (${CONSTANT_3})
  /** @var int ${CONSTANT_3} ${CONSTANT_3} flag. */
  const ${CONSTANT_3} = ${CONSTANT_3_VAL};
#end
#if (${CONSTANT_4})
  /** @var int ${CONSTANT_4} ${CONSTANT_4} flag. */
  const ${CONSTANT_4} = ${CONSTANT_4_VAL};
#end
#if (${CONSTANT_5})
  /** @var int ${CONSTANT_5} ${CONSTANT_5} flag. */
  const ${CONSTANT_5} = ${CONSTANT_5_VAL};
#end
#if (${CONSTANT_6})
  /** @var int ${CONSTANT_6} ${CONSTANT_6} flag. */
  const ${CONSTANT_6} = ${CONSTANT_6_VAL};
#end
#if (${CONSTANT_7})
  /** @var int ${CONSTANT_7} ${CONSTANT_7} flag. */
  const ${CONSTANT_7} = ${CONSTANT_7_VAL};
#end
#if (${CONSTANT_8})
  /** @var int ${CONSTANT_8} ${CONSTANT_8} flag. */
  const ${CONSTANT_8} = ${CONSTANT_8_VAL};
#end
}