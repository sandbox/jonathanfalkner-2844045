# PhpEnum
This project brings two php classes into Drupal to allow other modules to indicate dependency on these classes.

These classes provide basic Enum-like objects in PHP, allowing better hinting on bit flags or constants.

Sandbox project: https://www.drupal.org/sandbox/jonathanfalkner/2844045

#PHPStorm IDE
If you use the PHPStorm IDE, the file template provides a fast way to create Flag style enum files quickly.

To add this file template to PHPStorm, place it in the following directory:

* Windows: %USER_PROFILE%\.<product name><version number>\config\fileTemplates\
* Linux: ~/.<product name><version number>/config/fileTemplates/
* OS X: ~/Library/Preferences/<product name><version number>/fileTemplates/
