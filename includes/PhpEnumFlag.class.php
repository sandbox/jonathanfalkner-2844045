<?php
/**
 * Class PhpEnumFlag
 *
 * A class for bitmask enum flags.
 *
 * Assumes the values for the constants have been set as bitmask style flags.
 * Defining the ALL constant manually improves performance
 *
 * Usage:
 * class MyEnum extends PhpEnumFlag {
 *   const SOMETHING     = 0b00000001;
 *   const SOMETHINGELSE = 0b00000010;
 *   const YETANOTHER    = 0b00000100;
 *   const ALL           = 0b00000111;
 * }
 *
 * $t = MyEnum::fromMask(3);
 * $x = MyEnum::fromMask((MyEnum::SOMETHING | MyEnum::SOMETHINGELSE));
 * $y = MyEnum::SOMETHING();
 *
 * // TRUE - $y has the SOMETHING flag set.
 * $y->SOMETHING();
 *
 * // TRUE - $y is equal to the SOMETHING flag.
 * $y() == MySimpleEnumFlag::SOMETHING
 *
 * // TRUE - $x has the SOMETHING flag set.
 * $x->SOMETHING();
 *
 * // TRUE - $x has the SOMETHINGELSE flag set
 * $x->SOMETHINGELSE();
 *
 * // TRUE - $x is equal to the mask.
 * $x() == (MyEnum::SOMETHING | MyEnum::SOMETHINGELSE)
 * $x() == 3
 *
 * // TRUE - raw value of $x equals raw value of $t
 * $x() == $t()
 *
 * // TRUE - $x and $t are enums that have the same value. Be cautious with
 * // this! If type of $t is dynamic, the comparison will throw errors if the
 * // type is a non-string primitive, as php only supports implicit string
 * // conversion. It is safer to always use the $x() method for getting the raw
 * // value for comparison.
 * $x == $t
 *
 * For code completion of the magic methods, add
 * \@method [type] [method] [description]
 * \@method [type] is[method] [description]
 * \@method [type] has[method] [description]
 *
 * e.g.
 * \@method static SOMETHING() Get new instance of SOMETHING flag.
 * \@method bool isSOMETHING() Value flag exactly equals SOMETHING flag.
 * \@method bool hasSOMETHING() Value has SOMETHING flag bit set.
 *
 */
abstract class PhpEnumFlag extends PhpEnum {

  /**
   * Create an instance from an existing bitmask.
   *
   * Note: This will set the "label" for this mask to 'MASK', which may result
   * in incorrect comparisons if you are using string comparisons.
   *
   * @param $mask
   *   A bitmask to use.
   *
   * @return null|static
   */
  final public static function fromMask($mask) {
    if ($mask >= 0 && $mask <= static::ALL()) {
      return new static($mask, static::getLabel($mask));
    }
    else {
      return NULL;
    }
  }

  /**
   * Implements __call().
   *
   * Converts function name to a flag check against the enum's value.
   * Usage:
   * isERROR() checks if the enum's value is exactly the flag ERROR.
   * hasERROR() checks if the enum's value has the ERROR flag's bit set.
   */
  final public function __call($name, $arguments) {
    $return = FALSE;
    $is = FALSE;
    $has = FALSE;
    if (strlen($name) > 2) {
      $is = (substr($name, 0, 2) == 'is');
      $has = (!$is && substr($name, 0, 3) == 'has');
      $const_name = ($is ? substr($name, 2) : $has ? substr($name, 3) : '');
      if ($name) {
        $constant = static::class . '::' . $const_name;
        // If function called matches the name of a constant, check if $value
        // bitmap contains the flag.
        if (defined($constant)) {
          if ($is) {
            $return = $this->is($constant());
          }
          elseif ($has) {
            $return = $this->has($constant());
          }
        }
      }
    }
    // If method call was valid with an is or has prefix and the constant exists,
    // return whether or not the flag met the mask requirements.
    if ($is || $has) {
      return $return;
    }
    else {
      // Throw an error so the dev knows they referenced an invalid method.
      trigger_error('Call to undefined method ' . __CLASS__ . '::' . $name . '()', E_USER_ERROR);
      return NULL;
    }
  }

  /**
   * Check if value of enum exactly matches passed in enum object.
   *
   * @param PhpEnumFlag $flag
   * @return bool
   */
  public function is(PhpEnumFlag $flag){
    return ($this->value & $flag()) == $this->value;
  }

  /**
   * Check if value of enum has the flag bit set.
   *
   * @param PhpEnumFlag $flag
   * @return bool
   */
  public function has(PhpEnumFlag $flag){
    return (bool) ($this->value & $flag());
  }

  /**
   * @param $flag
   *   Bitmask for which to retrieve a label.
   *
   * @return string
   *   Label of flag if $flag matches a flag's value exactly, else an empty
   *   string.
   */
  public static function getLabel($flag) {
    static $labels;
    if (!isset($labels)) {
      $all = static::getConstants();
      $labels = array_flip($all);
    }

    return array_key_exists($flag, $labels) ? $labels[$flag] : '';
  }

  /**
   * Retrieve a bitmask with all bits set.
   *
   * If a constant of ALL is defined, it overrides this value.
   *
   * @return int
   */
  public static function ALL(){
    static $all_mask;
    if (!isset($all_mask)) {
      // If class defines ALL property, use it.
      if (defined(static::class . '::ALL')) {
        $all_mask = static::ALL;
      }
      // Else generate an ALL result by combining all flags.
      else {
        $constants = static::getConstants();
        $all_mask = 0;
        /** @var PhpEnumFlag $flag */
        foreach ($constants as $flag_name => $flag_value) {
          $all_mask |= $flag_value;
        }
      }
    }
    return $all_mask;
  }
}
