<?php
/**
 * Class PhpEnum
 *
 * A simple enum for php.
 *
 * Usage:
 * @code
 * MyEnum extends PhpEnum {
 *   const SOME_VALUE = 'test;
 *   const SOME_VALUE2 = 'test2';
 * }
 *
 * $x = MyEnum::SOME_VALUE();
 * $y = MyEnum::SOME_VALUE();
 *
 * For comparisons, the enum object must be reduced to its raw value first by
 * invoking the object directly:
 *
 * $x() == 12 // FALSE
 * $x() == MyEnum::SOME_VALUE // TRUE
 * $x() == 'test' // TRUE
 * $x() == $y();
 *
 * Casting to string will get you the label of the constant used instead of the
 * value:
 *
 * $x == 'SOME_VALUE' // TRUE
 *
 * For code completion of the magic methods, add
 * \@method [type] SOME_VALUE() Description
 *
 * @endcode
 */
abstract class PhpEnum {
  protected $value;
  protected $label;

  /**
   * Create an instance of a PhpEnum derived object and set the enum value.
   *
   * Final protected constructor to enforce instantiation via static methods
   * to ensure that $value is always a valid value.
   *
   * @param $label
   *   The text equivalent of the constant that the enum should represent.
   */
  final protected function __construct($value, $label) {
    $this->value = $value;
    $this->label = $label;
  }

  /**
   * Implements __callStatic().
   *
   * Converts static method call to an instantiation of the enum class using
   * the constant of the same name as the value of the Enum.
   */
  final public static function __callStatic($name, $arguments) {
    $constant = static::class . '::' . $name;
    if (defined($constant)) {
      return new static(constant($constant), $name);
    }
    else
    {
      // Throw an error so the dev knows they referenced an invalid method.
      trigger_error('Call to undefined method ' . __CLASS__ . '::' . $name . '()', E_USER_ERROR);
      return NULL;
    }
  }

  /**
   * Implements __toString().
   *
   * @return string
   *   The name of the constant that the enum's current value represents.
   */
  final public function __toString()
  {
    return (string)$this->label;
  }

  /**
   * Retrieve a list of all constants defined on the current object.
   *
   * @return array
   */
  protected static function getConstants() {
    static $constants;
    if (!isset($constants)) {
      $o = new ReflectionClass(static::class);
      $constants = $o->getConstants();
    }
    return $constants;
  }

  /**
   * Retrieve the raw value of the enum to use in comparisons.
   * @return mixed
   */
  final public function __invoke()
  {
    return $this->value;
  }
}
